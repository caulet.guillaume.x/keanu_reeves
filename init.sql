DROP DATABASE IF EXISTS Keanu;


CREATE DATABASE Keanu;

USE Keanu;

CREATE TABLE film (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    titre VARCHAR(30) NOT NULL,
    personnage VARCHAR(30) NOT NULL,
    imgURL VARCHAR(255) NOT NULL
);
GRANT ALL PRIVILEGES ON Keanu.* TO 'admin'@'localhost';

INSERT INTO film (titre,personnage,imgURL)
VALUES ("Matrix","Thomas 'Néo' Anderson","https://movieposters2.com/images/1566583-b.jpg");

SELECT * FROM film;