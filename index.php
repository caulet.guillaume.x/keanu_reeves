<?php include 'header.php'?>
        <div id="container">
            <?php 
            include 'data.php';
            $list_film = getAllFilm();
            ?>
            <?php foreach($list_film as $film){
            ?>

                <div id="film" class="card">
                    <img src="<?php echo $film['imgURL']; ?>" alt="Keanu">

                    <h2><?php echo $film['titre']; ?></h2>

                    <p><?php echo $film['personnage']; ?></p>
                </div>
            <?php } ?>
        </div> 
<?php include 'footer.php'?>
